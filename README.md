# Ansible for configuration of MPLS option A and B on Cisco IOS

- `initial_configs` - contains basic configs on Cisco IOS devices that allows for Mgmt VRF and SSH access. Also `CML/Virl2` lab template is included.

- `mpls-A` - provides MPLS option A via `cisco.ios.ios_config` module, however, this is rather best effort than best practice deployment. This can be implemented to quickly check proper config.

- `mpls-B` - provides MPLS option B like above.

- `playbooks` - this is best practice MPLS deployment based on the roles and appropriate modules for each functionality. These playbooks follow best practice guidelines and are developed considering DevNetOps approach - should be easily integrated into CICD systems. This is currently WIP and still not tested.

- `requirements.yml` - contains dependencies for notebooks from `ansible-galaxy`.
